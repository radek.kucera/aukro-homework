﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using aukro.Model;

namespace aukro.ViewModel
{
    class UserViewModel
    {
        private UserModel user;
        public UserModel User
        {
            get
            {
                return user;
            }

            set
            {
                if (user != value)
                {
                    user = value;
                }
            }
        }

        public UserViewModel()
        {
            user = new UserModel();
        }
    }
}
