﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aukro.Model;

namespace aukro.ViewModel
{
    class ItemViewModel
    {
        public List<ItemModel> ItemModel;

        public ItemViewModel()
        {
            //this.LabelText = "ASdf";
            ItemModel = initItemModelCollection();
        }

        private static List<ItemModel> initItemModelCollection()
        {
            List<ItemModel> items = new List<ItemModel>();

            foreach (Item item in Items.GetItems())
            {
                items.Add(new ItemModel(item));
            }

            return items;
        }

        //public string LabelText { get; set; }
    }
}
