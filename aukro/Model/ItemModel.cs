﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace aukro.Model
{
    class ItemModel : INotifyPropertyChanged
    {
        public readonly Item Info;

        private int currentBid;
        public int CurrentBid
        {
            get
            {
                return currentBid;
            }

            set
            {
                if (currentBid != value)
                {
                    currentBid = value;
                    OnPropertyChanged("currentBid");
                }
            }
        }
        public ItemModel(Item item)
        {
            this.Info = item;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
