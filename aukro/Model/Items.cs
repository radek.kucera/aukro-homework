﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aukro.Model
{
    public class Items
    {
        private static List<Item> itemsDb = initDb();

        public static List<Item> GetItems()
        {
            return itemsDb;
        }

        private static List<Item> initDb()
        {
            List<Item> items = new List<Item>();

            items.Add(new Item(0, "Počítač", "Stolní počítač bílý", Users.checkUser("admin", "admin"), 1200, 200));

            return items;
        }
    }
}
