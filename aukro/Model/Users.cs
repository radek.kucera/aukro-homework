﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aukro.Model
{
    public class Users
    {
        //Kolekce uživatelů - reprezentuje databázi
        private static List<User> UsersDb = initDb();

        //Přihlášení uživatele - vyhledání v kolekci
        public static User checkUser(string username, string password)
        {
            foreach(User dbUser in UsersDb)
            {
                if (dbUser.Password == password && dbUser.Username == username) return dbUser;
            };

            return null;
        }
 

        //Vytvoření uživatelů
        private static List<User> initDb()
        {
            List<User> users = new List<User>();

            users.Add(new User(0, "admin", "admin"));
            users.Add(new User(1, "root", "toor"));
            users.Add(new User(2, "user", "user"));

            return users;
        }
    }
}
