﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace aukro.Model
{
    public class Item
    {
        private User user;
        public string User
        {
            get
            {
                return user.Username;
            }
        }

        public readonly string Title;

        public readonly string Description;

        public readonly int BuyNow;

        public readonly DateTime ExperiedTime;

        public readonly int MinBid;

        public readonly int Id;

        public Item(int id, string title, string description, User user, int buyNow, int minBid)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.user = user;
            this.BuyNow = buyNow;
            this.ExperiedTime = setExperiedTime(new DateTime());
            this.MinBid = minBid;
        }

        private DateTime setExperiedTime(DateTime day)
        {
            day.AddDays(14);
            return day;
        }
    }
}
